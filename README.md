# Inline SVG proof of concept

I don't really like that we use font icons for so many things. They're hard to make accessible, kind of a pain to work with, if they don't work correctly they show up as ☐'s...

It would be nice to use inline svgs though! There are a couple downsides though
- they (can) add a ton of markup to the page
- they aren't stored in an assets folder
- they're "per project" (you have to get them from somewhere every time you want to make something)

With the `<use>` tag, you can reference an svg that's in a separate folder

```html
<svg width="48" height="32">
  <use href="/icons/facebook.svg#facebook" fill="red"/>
</svg>
```

You could make a whole sheet of icons like this and then reference them by their id's as well.

But that doesn't solve the last problem. You still need to copy/paste them into each project. The `<use>` tag `href` property can't fetch assets from a CDN for instance.

To solve that problem you can use gulp though. The `gulp-download2` package lets you download files as part of a gulp task (see gulpfile.js).

That means you can reference the icons reliably locally but not keep them in source control. When designers make an update to the icons on the CDN, updating icons in production is straightforward. All that needs to be done is re-run the same gulp task again.

## IE support
IE is annoying...

The way to make this work is with a helper like the [svg4everybody](https://github.com/jonathantneal/svg4everybody) script. This will give support down to IE9.
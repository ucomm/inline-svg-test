const gulp = require('gulp')
const download = require('gulp-download2')

const url = [
  `https://files-universityofconn.netdna-ssl.com/shared/tmp/facebook.svg`,
  `https://files-universityofconn.netdna-ssl.com/shared/images/uconn.svg`
]

const dl = () => {
  return download(url)
    .pipe(gulp.dest('icons'))
}

gulp.task('download', gulp.series(dl))
